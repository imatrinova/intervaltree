﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntervalTree
{
    public class Interval<T>: IComparable<T> , IEquatable<T>
        where T : IComparable
    {
        public T StartPoint {get;}

        public T EndPoint { get; }

        public Interval(T startPoint, T endPoint)
        {
            StartPoint = startPoint;
            EndPoint = endPoint;
        }

        public int CompareTo(T other)
        {
            return this.CompareTo(other);
        }

        public bool Equals(T other)
        {
            return this.Equals(other);
        }        
    }
}
