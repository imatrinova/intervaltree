﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace IntervalTree
{
    static class IntervalTreeCreator<T>
        where T : IComparable
    {
        public static Leaf<T> GetLeaf(IEnumerable<Interval<T>> intervals)
        {
            if (intervals is null || intervals.Count() == 0) return null;
            var res = new Leaf<T>();
            res.Point = GetMedianPoint(intervals);
            res.Intervals = intervals.Where(x => x.ContainsPoint(res.Point));
            res.RightLeaf = GetLeaf(intervals.Where(x => x.GreaterThenPoint(res.Point)));
            res.LeftLeaf = GetLeaf(intervals.Where(x => x.LessThenPoint(res.Point)));
            return res;
        }

        private static T GetMedianPoint(IEnumerable<Interval<T>> intervals)
        {
            var oderedPoints = GetOderedPoints(intervals);
            var res = oderedPoints.ToList<T>();
            return res[res.Count / 2];

        }

        private static IEnumerable<T> GetOderedPoints(IEnumerable<Interval<T>> intervals)
        {
            var pointsList = intervals.Select(x => x.StartPoint).ToList();
            pointsList.AddRange(intervals.Select(x => x.EndPoint));
            var points = pointsList.ToHashSet();

            return points.OrderBy(x => x);
        }
    }
}
