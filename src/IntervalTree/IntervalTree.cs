﻿using System;
using System.Collections.Generic;

namespace IntervalTree
{
    public class IntervalTree<T>
         where T : IComparable
    {
        Leaf<T> Root { get; }
        public IntervalTree(IEnumerable<Interval<T>> intervals)
        {
            Root = IntervalTreeCreator<T>.GetLeaf(intervals);
        }

        public IEnumerable<Interval<T>> GetInrevalsByPoint(T point)
        {
            return IntervalTreeAnalyzer<T>.GetIntervalsByPoint(point, Root);
        }
    }
}
