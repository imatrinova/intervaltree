﻿using System;
using System.Collections.Generic;

namespace IntervalTree
{
    class Leaf<T>
         where T : IComparable
    {
        public T Point { get; set; }
        public IEnumerable<Interval<T>> Intervals;
        public Leaf<T> LeftLeaf;
        public Leaf<T> RightLeaf;
    }
}
