﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace IntervalTree
{
    static class IntervalExtension
    {
        public static bool ContainsPoint<T>(this Interval<T> interval, T point) where T:IComparable
        {
            return (interval.StartPoint.CompareTo(point) <= 0) && (interval.EndPoint.CompareTo(point) >= 0);
        }

        public static bool GreaterThenPoint<T>(this Interval<T> interval, T point) where T : IComparable
        {
            return (interval.StartPoint.CompareTo(point) > 0);
        }

        public static bool LessThenPoint<T>(this Interval<T> interval, T point) where T : IComparable
        {
            return (interval.EndPoint.CompareTo(point) < 0);
        }
    }
}
