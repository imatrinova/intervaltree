﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace IntervalTree
{
    static class IntervalTreeAnalyzer<T>
       where T : IComparable
    {

        internal static IEnumerable<Interval<T>> GetIntervalsByPoint(T point, Leaf<T> root)
        {
            var res = new List<Interval<T>>();
            if (root is null) return res;
            if (root.Point.Equals(point)) return root.Intervals;
            res.AddRange(root.Intervals.Where(x => x.ContainsPoint(point)).ToList());
            if (point.CompareTo(root.Point) > 0) res.AddRange(GetIntervalsByPoint(point, root.RightLeaf));
            else res.AddRange(GetIntervalsByPoint(point, root.LeftLeaf));
            return res;
        }
    }
}
