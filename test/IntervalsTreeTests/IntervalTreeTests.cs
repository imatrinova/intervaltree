using IntervalTree;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace IntervalsTreeTests
{
    public class IntervalTreeTests
    {
        [Fact]
        public void ReturnNullForEmptyIntervals()
        {
            var intervals = new List<Interval<int>>();
            var intervalTree = new IntervalTree<int>(intervals);
            var res = intervalTree.GetInrevalsByPoint(1);
            Assert.Empty(res);
        }

        [Fact]
        public void ReturnNotNullForIntIntervals()
        {
            var intervals = new List<Interval<int>>() { new Interval<int>(2, 6), new Interval<int>(4, 8), new Interval<int>(2, 9), new Interval<int>(10, 18) };
            var intervalTree = new IntervalTree<int>(intervals);
            var res = intervalTree.GetInrevalsByPoint(5).ToList();
            Assert.NotEmpty(res);
            Assert.True(res.Count == 3);
        }

        [Fact]
        public void ReturnNotNullForDateTimeIntervals()
        {
            var intervals = new List<Interval<DateTime>>() { new Interval<DateTime>(new DateTime(2000,12,01), new DateTime(2005, 12, 01)),
                new Interval<DateTime>(new DateTime(2006,12,01), new DateTime(2009, 12, 01)),
                new Interval<DateTime>(new DateTime(2001,12,01), new DateTime(2004, 12, 01)) };
            var intervalTree = new IntervalTree<DateTime>(intervals);
            var res = intervalTree.GetInrevalsByPoint(new DateTime(2003, 12, 01)).ToList();
            Assert.NotEmpty(res);
            Assert.True(res.Count == 2);
        }
    }
}
